package com.pisano;

import javax.swing.*;
import java.awt.*;
import javax.swing.Timer;

class Thank extends JFrame {

  Thank() {
    this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    this.setUndecorated(true);
    this.pack();
    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    this.createLabel();
    this.setVisible(true);
  }

  private void createLabel() {
    JLabel thankLabel = new JLabel("Tesekkurler", SwingConstants.CENTER);
    thankLabel.setFont(new Font(thankLabel.getName(), Font.BOLD, 46));
    add(thankLabel, BorderLayout.CENTER);
    this.sendThankPage();
  }

  private void sendThankPage() {
    Timer timer = new Timer(6000, e -> {
      this.dispose();
    });
    timer.start();
  }
}
