package com.pisano;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MainPanel extends JPanel {

  private JButton happyButton;
  private JButton sadButton;

  public MainPanel() {
    this.setBackground(Color.BLUE);
    this.setLayout(new BorderLayout());
    this.createButtons();
    this.setFocusable(true);
    this.requestFocusInWindow();
    this.setVisible(true);
  }

  private void createButtons() {
    ImageIcon happyIcon = createImageIcon("images/happy.png");
    happyButton = new JButton(happyIcon);
    happyButton.setBackground(SystemColor.control);
    happyButton.addActionListener(new PisanoClickAction());
    happyButton.addKeyListener(new PisanoKeyAction());

    ImageIcon sadIcon = createImageIcon("images/sad.png");
    sadButton = new JButton(sadIcon);
    sadButton.setBackground(SystemColor.control);
    sadButton.addActionListener(new PisanoClickAction());
    sadButton.addKeyListener(new PisanoKeyAction());


    JPanel buttonGroup = new JPanel(new GridLayout(1,2));

    buttonGroup.add(happyButton);
    buttonGroup.add(sadButton);

    add(buttonGroup, BorderLayout.CENTER);
  }

  protected static ImageIcon createImageIcon(String path) {
    java.net.URL imgURL = MainPanel.class.getResource(path);
    return new ImageIcon(imgURL);
  }

  public class PisanoClickAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {}
  }

  private class PisanoKeyAction implements KeyListener {

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
      if(e.getKeyCode() == KeyEvent.VK_A) {
        happyButton.setBackground(Color.CYAN);
        sadButton.setBackground(Color.WHITE);
        new Thank();
      } else if(e.getKeyCode() == KeyEvent.VK_B) {
        sadButton.setBackground(Color.CYAN);
        happyButton.setBackground(Color.WHITE);
        new Thank();
      } else {
        happyButton.setBackground(Color.WHITE);
        sadButton.setBackground(Color.WHITE);
        happyButton.setForeground(Color.black);
        sadButton.setForeground(Color.black);
      }
    }

    @Override
    public void keyReleased(KeyEvent e) {}
  }
}
