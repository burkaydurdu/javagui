package com.pisano;

import javax.swing.*;

public class Home  extends JFrame {

    private MainPanel mainPanel;
    public Home() {
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setUndecorated(true);
        this.createPanel();
        this.pack();
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void createPanel() {
        mainPanel = new MainPanel();
        setContentPane(mainPanel);
    }
}
